import WAV
import MFCC
import GaussianMixtures
using JLD
using IVectors
using FileIO
using PDMats
using MultivariateStats

import JSON

struct GMMInfo 
  name::AbstractString
  path::AbstractString
  gmm::GaussianMixtures.GMM
end

struct SVInfo 
  name::AbstractString
  sv::Vector
end

struct IVecInfo 
  name::AbstractString
  ivec::Vector
end

gmms = Array{GMMInfo,1}()
supervectors = Array{SVInfo,1}()
ivectors = Array{IVecInfo,1}()
#pcaModel = MultivariateStats.PCA()
whiteningModel = MultivariateStats.Whitening

function wav2gmm(name::AbstractString, path::AbstractString, n::Int)
    y, fs = WAV.wavread(path)
    samples = y[:,1]
    mfccs, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
	println(size(mfccs))
    gmm = GaussianMixtures.GMM(n, mfccs, nInit=200)
    GMMInfo(name, path, gmm)
end

function sampling(name::AbstractString)
	y, sampling_frequency = WAV.wavread(path)
	samples = Array{Array,1}()
	number_of_seconds = (length(y)/sampling_frequency)
	
	for second = 0:number_of_seconds
		start = second*sampling_frequency
		#end = (second*sampling_frequency)+sampling_frequency
		#samples[second] = y[start:end]
	end
	samples
end

function cluster(k::Integer, name::AbstractString)
	samples = sampling(name)
	ie = load("IExtractor3.iex")
	cut_off = True
	cluster_ivectors = Array{vector,1}()
	for sample in sampling
		mfccs, power, info= MFCC.feacalc(sample, wintime=0.025, steptime=0.01, normtype=:mvn, augtype=:delta)
		mfccs = convert(Array{Float64, 2}, mfccs)
		ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfccs))
		pca_vec = Array{Float64,2}()
		pca_vec = reshape(ivec,1,length(ivec))
		pcaModel = fitPca(PCA, pca_vec; maxoutdim=100, mean=0)
		pca_vec = transform(pcaModel, pca_vec)
		push!(cluster_ivectors, vec(pca_vec))
	end
	
	while(cut_off)
		max_score = 0
		max_score_pos = [0,0]
		for current_vector in cluster_ivectors
			for vector in cluster_ivectors
				score = cosdist(current_vector, vector)
				
				if score > max_score
					max_score = score
					max_score_pos = [findfirst(cluster_ivectors,current_vector), findfirst(cluster_ivectors,vector)]
				end
			end
		end
		samples[max_score_pos[1]] = append(max_score_pos[1], max_score_pos[2])
		#Kode for å fjerne sample som blei appenda og fjerne i vec som tilhører samplet
		if length(samples <= k)
			cut_off = False
		end
	end
end

## For this function to work you have to be in the folder Kode-Master.
function make_ubm(N::Int=1024, name::String="ubm-alt-1024-delta-mvn-stand", filename::String="ubm-alt-1024-delta-mvn-stand.jld", gmms::Array{GMMInfo}=gmms)
	cd("ubm-alt")
	file_list = readdir()
	mfcc_list = []
	println(file_list)
	for file in file_list
		y, fs = WAV.wavread(file)
		println(fs)
		samples = y[:,1]
		samples = (samples - mean(samples))/std(samples)
		#mfccs, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
		mfccs, power, info= MFCC.feacalc(samples; wintime=0.025, steptime=0.01, augtype=:delta, normtype=:mvn, numcep=13, defaults=:rasta)
		#mfccs, power, info= MFCC.feacalc(file, :diarization, wintime=0.025, steptime=0.01, augtype=:delta, normtype=:mvn)
		mfccs = convert(Array{Float64, 2}, mfccs)
		if isempty(mfcc_list)
			mfcc_list = mfccs
		else
			mfcc_list = vcat(mfccs, mfcc_list)
		end
	end
	gmm = GaussianMixtures.GMM(N, mfcc_list, nInit=100000)
	gmminfo = GMMInfo("UBM", "", gmm)
	cd("..")
	push!(gmms, gmminfo)
	save(filename, name, gmm)
	gmms
end

function train_GMM(N::Int, json_path::AbstractString="json.txt", gmms::Array{GMMInfo}=gmms)
    #gmms = make_ubm(40)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
    for speaker in json
        gmm = wav2gmm(speaker[1], speaker[3], N)
        push!(gmms, gmm)
    end
    gmms
end

function train_UBMGMM(json_path::AbstractString="json.txt", gmms::Array{GMMInfo}=gmms)
	#gmms = make_ubm("size=1000, r=16, all clips from skogbrann", 1000)
	gmminfo = GMMInfo("UBM", "", load("saved-ubm(513).jld", "size=513, r=16, all clips from skogbrann"))
	push!(gmms, gmminfo)
	println(gmms[1].gmm)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
	for speaker in json
		y, fs = WAV.wavread(speaker[3])
		samples = y[:,1]
		mfccs, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
		gmm = GaussianMixtures.maxapost(gmms[1].gmm, mfccs, 16, means=true, weights=true, covars=true)
		gmminfo = GMMInfo(speaker[1], "", gmm)
		push!(gmms, gmminfo)
	end
	gmms
end

function inference(gmmmodels::Array{GMMInfo}=gmms, path::AbstractString="clip_0.wav")
    y, fs = WAV.wavread(path)
    samples = y[:,1]
    mfccs, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)    
    scores = map((gmminfo)->GaussianMixtures.avll(gmminfo.gmm, mfccs), gmmmodels)
    pos = indmax(scores)
    gmmmodels[pos].name, scores[pos], scores
end

function test_UBMGMM(json_path::AbstractString="json_test.txt", gmms::Array{GMMInfo}=gmms)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
	for speaker in json
		name, score, scores_list = inference(gmms, speaker[3])
		if name == speaker[1]
			println("KORREKT!!, navn: ", name)
		else
			println("Feil! Klassifisert som: ", name)
			println("Korrekt svar er: ", speaker[1])
			println("Score list ", scores_list)
			
		end
	end
end

function train_supervec(json_path::AbstractString="json.txt", N::Int=16, gmms::Array{GMMInfo}=gmms)
	#gmms = make_ubm(100)
	#gmminfo = GMMInfo("UBM", "", load("saved-ubm(fraag+skog).jld", "size=600, fraagstund+noko skogbrann"))
	#gmminfo = GMMInfo("UBM", "", load("ubm-alt.jld", "ubm-alt"))
	gmminfo = GMMInfo("UBM", "", load("ubm-alt-ny.jld", "ubm-alt-ny"))
	push!(gmms, gmminfo)
	#y, fs = WAV.wavread("clip_0.wav")
	#samples = y[:,1]
	#mfccs, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
	#sv = Base.vec(gmms[1].gmm, mfccs, 0)
	#svinfo = SVInfo("SV-UBM", sv)
	#push!(supervectors, svinfo)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
	for speaker in json
		y, fs = WAV.wavread(speaker[3])
		samples = y[:,1]
		mfccs, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
		sv = Base.vec(gmms[1].gmm, mfccs, N)
		svinfo = SVInfo(speaker[1], sv)
		push!(supervectors, svinfo)
	end
end

function classify_sv(path::AbstractString, svmodels::Array{SVInfo}=supervectors)
	y, fs = WAV.wavread(path)
    samples = y[:,1]
    mfccs, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
	unknown_sv = Base.vec(gmms[1].gmm, mfccs)
	scores_list = Array{Float64,1}()
	for vector in supervectors
		if vector.name != "SV-UBM"
			#vec1 = vector.sv - supervectors[1].sv
			#vec2 = unknown_sv - supervectors[1].sv
			#score = (vecdot(vec1, vec2))/(norm(vec1)*norm(vec2))
			score = (vecdot(vector.sv, unknown_sv))/(norm(vector.sv)*norm(unknown_sv))
			push!(scores_list, score)
		end
	end
    pos = indmax(scores_list)
    svmodels[pos].name, scores_list[pos], scores_list
end

function test_SV(json_path::AbstractString="json_test.txt", svmodels::Array{SVInfo}=supervectors)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
	for speaker in json
		name, score, scores_list = classify_sv(speaker[3], svmodels)
		if name == speaker[1]
			println("KORREKT!!, navn: ", name, " Score: ", score)
			println("Score list ", scores_list)
			println(" ")
		else
			println("Feil! Klassifisert som: ", name, "Score: ", score)
			println("Korrekt svar er: ", speaker[1])
			println("Score list ", scores_list)
			println(" ")
		end
	end
end

function train_ivecex()
	gmminfo = GMMInfo("UBM", "", load("ubm-alt-1024-delta.jld", "ubm-alt-1024-delta"))
	push!(gmms, gmminfo)
	cd("ubm-alt")
	#cd("ubm-fraagstund+noko-skogbrann")
	file_list = readdir()
	mfcc_list = []
	for file in file_list
		#y, fs = WAV.wavread(file)
		#samples = y[:,1]
		#mfccs, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
		mfccs, power, info= MFCC.feacalc(file, :diarization, wintime=0.025, steptime=0.01, augtype=:delta)
		mfccs = convert(Array{Float64, 2}, mfccs)
		push!(mfcc_list, mfccs)
	end
	cd("..")
	css = map((data)->GaussianMixtures.CSstats(gmms[1].gmm, data), mfcc_list)
	nvoices = 400
	ie = nothing
	try
		ie = IVectors.IExtractor(css, nvoices; nIter=5)
	catch
		train_ivecex()
	end
	save("IExtractor3.iex", ie)
end

function make_ivector_extractor(ubm::GMMInfo=GMMInfo("UBM", "", load("ubm-alt-1024-delta.jld", "ubm-alt-1024-delta")), ubmdir::String="ubm-alt"; ncomp=400)
    cd(ubmdir)
	filename_list = readdir()
    mfcc_list = []
	model_training_mfcc = Array{Float64,2}()
	println(typeof(model_training_mfcc))
    for filename in filename_list
		print(filename)
        mfccs, power, info= MFCC.feacalc(filename, :diarization, wintime=0.025, steptime=0.01, augtype=:delta)
		mfccs = convert(Array{Float64, 2}, mfccs)
        push!(mfcc_list, mfccs)
    end
	print("før try")
	cd("..")
	css = map((data)->GaussianMixtures.CSstats(ubm.gmm, data), mfcc_list)
    ie = IVectors.IExtractor(css, ncomp; nIter=6)
	save("IExtractor3.iex", ie)
end


function train_ivec(json_path::AbstractString="json.txt", gmms::Array{GMMInfo}=gmms)
	ie = load("IExtractor3.iex")
	#gmminfo = GMMInfo("UBM", "", load("saved-ubm(fraag+skog).jld", "size=600, fraagstund+noko skogbrann"))
	#gmminfo = GMMInfo("UBM", "", load("ubm-alt.jld", "ubm-alt"))
	gmminfo = GMMInfo("UBM", "", load("ubm-alt-1024-delta.jld", "ubm-alt-1024-delta"))
	push!(gmms, gmminfo)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
	for speaker in json
	println("mfccs begin")
		y, fs = WAV.wavread(speaker[3])
		samples = y[:,1]
		#mfccs2, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
		mfccs, power, info= MFCC.feacalc(speaker[3], :diarization, wintime=0.025, steptime=0.01, augtype=:delta)
		mfccs = convert(Array{Float64, 2}, mfccs)
		ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(gmminfo.gmm, mfccs))
		#ivec = transform(pcaModel, ivec)
		iv = IVecInfo(speaker[1], ivec)
		push!(ivectors, iv)
	end
end

function make_ivector(ubm, ie::IVectors.IExtractor, path::AbstractString="json.txt")
    #y, fs = WAV.wavread(path)
    #samples = y[:,1]
    mfccs, power, info= MFCC.feacalc(path, :diarization, wintime=0.025, steptime=0.01, augtype=:delta)
	mfccs = convert(Array{Float64, 2}, mfccs)
    ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfccs))
end

function make_ivectors(ie::IVectors.IExtractor = load("IExtractor3.iex"), json_path::AbstractString="json.txt")
	ubm = GMMInfo("UBM", "", load("ubm-alt-1024-delta.jld", "ubm-alt-1024-delta"))
    f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
    #ivectors = []
    for speaker_rec in json
        speaker = speaker_rec[1]
        println(speaker)
        ivec = make_ivector(ubm, ie, speaker_rec[3])
		pca_vec = Array{Float64,2}()
		pca_vec = reshape(ivec,1,length(ivec))
		pcaModel = fitPca(PCA, pca_vec; maxoutdim=100, mean=0)
		pca_vec = transform(pcaModel, pca_vec)
		ivecinf = IVecInfo(speaker, vec(pca_vec))
        #ivecinf = IVecInfo(speaker, ivec)
        push!(ivectors, ivecinf)
    end
    ivectors
end

function test_IV(json_path::AbstractString="json_test.txt", ivmodels::Array{IVecInfo}=ivectors)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
	perc = 0
	for speaker in json
		name, score, scores_list = classify_iv(speaker[3], ivmodels)
		if name == speaker[1]
			println("KORREKT!!, navn: ", name, " Score: ", score)
			println("Score list ", scores_list)
			println(" ")
			perc += 1
		else
			println("Feil! Klassifisert som: ", name, " Score: ", score)
			println("Korrekt svar er: ", speaker[1])
			println("Score list ", scores_list)
			println(" ")
		end
	end
	println("antall korrekt er: ", perc)
	println("Prosent korrekt er: ", (perc/length(json)))
	println(length(json))
end

function classify_iv(path::AbstractString, ivmodels::Array{IVecInfo}=ivectors)
	ie = load("IExtractor3.iex")
	ubm = GMMInfo("UBM", "", load("ubm-alt-1024-delta.jld", "ubm-alt-1024-delta"))
	#y, fs = WAV.wavread(path)
    #samples = y[:,1]
	mfccs, power, info= MFCC.feacalc(path, :diarization, wintime=0.025, steptime=0.01, augtype=:delta)
	mfccs = convert(Array{Float64, 2}, mfccs)
	unknown_ivec = ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfccs))
	pca_vec = Array{Float64,2}()
	pca_vec = reshape(unknown_ivec,1,length(unknown_ivec))
	pcaModel = fitPca(PCA, pca_vec; maxoutdim=100, mean=0)
	pca_vec = transform(pcaModel, pca_vec)
	scores_list = Array{Float64,1}()
	for vector in ivmodels
		#score = (dot(vector.ivec, unknown_ivec))/(norm(vector.ivec)*norm(unknown_ivec))
		#score = 1 - dot(vector.ivec[1], unknown_ivec[1]) / (norm(vector.ivec) * norm(unknown_ivec))
		#score = cosdist(vector.ivec, unknown_ivec)
		score = cosdist(vector.ivec, vec(pca_vec))
		#println(score)
		push!(scores_list, score)
	end
    pos = indmax(scores_list)
    ivmodels[pos].name, scores_list[pos], scores_list
end