
import WAV
import MFCC
using HypothesisTests

# Pool covariance matrices, overwriting the first
function poolcov!(Sx::AbstractMatrix, nxm1::Int, Sy::AbstractMatrix, nym1::Int)
    @inbounds for i = eachindex(Sx, Sy)
        Sx[i] = (Sx[i] * nxm1 + Sy[i] * nym1) / (nxm1 + nym1)
    end
    Sx
end

# Helper function for computing A'B⁻¹A when B is a covariance matrix
At_Binv_A(A::AbstractArray, B::AbstractArray) = A'*(B \ A)

function wav2mfcc(path::AbstractString)
    y, fs = WAV.wavread(path)
    sample = y[:,1]
    sample = (sample - mean(sample))/std(sample)
    mfccs, power, info = MFCC.mfcc(sample, fs, wintime=0.025, steptime=0.020, nbands=40, numcep=24)
    #mfccs = (mfccs - mean(mfccs))/std(mfccs)
    N, d = size(mfccs)
    println("TIME ", N*0.020)
    mfccs 
end

function Ri(mfccs::Array{Float64,2},
            from::Int64, to::Int64, pos::Int64)
    #N = to - from
    #N1 = pos - from
    #N2 = to - pos
    N = to
    N1 = pos
    N2 = (to - pos)
    Σ = cov(mfccs[from:to, :])
    Σ1 = cov(mfccs[from:pos, :])
    Σ2 = cov(mfccs[pos+1:to, :])
    L =  N * log(abs(det(Σ)))
    L1 = N1 * log(abs(det(Σ1)))
    L2 = N2 * log(abs(det(Σ2)))
    #println("--->", "Σ=", Σ, ",Σ1=", Σ1, ",Σ2=", Σ2, "L=", L, ",L1=", L1, ",L2=", L2)
    println([from:to, from:pos, pos:to])
    0.5*(L - L1 - L2)
end

function BIC(mfccs::Array{Float64,2},
             from::Int64, to::Int64, pos::Int64)
    N, d = size(mfccs)

    if to > N
        to = N
    end

    if from < 1
        from = 1
    end
    d = Float64(size(mfccs)[2])
    const λ = 1.3
    P = λ*0.5(d + 0.5*d*(d+1)) * log(to)
    Ri(mfccs, from, to, pos) - P
end

function t2(mfccs::Array{Float64,2},
            from::Int64, to::Int64, pos::Int64)
    X = mfccs[from:pos, :]
    Y = mfccs[pos+1:to, :]
    nx, p = size(X)
    ny, q = size(Y)
    Δ = vec(mean(X, 1) .- mean(Y, 1))
    Σ = poolcov!(cov(X), nx - 1, cov(Y), ny - 1) .* (inv(nx) .+ inv(ny))
    #T = ((pos*(to-pos))/to) * transpose(mean(X) - mean(Y)) * inv(Σ) * (mean(X) - mean(Y))
    T = At_Binv_A(Δ, Σ)
    return T     
end

function t2_window(mfccs::Array{Float64,2},
            from::Int64, to::Int64)
    best = 100
    bestpos = 0
    scores = Array{AbstractFloat,1}()
    N, d = size(mfccs)
    if to > N
        to = N
    end

    if from < 1
        from = 1
    end
    for i = from+1:to-2
        b = t2(mfccs, from, to, i)
        #println(i, " ", b)
        push!(scores, b)
    end
    x = from:to
    println("T WINDOW ", x)
    return  bestpos, scores
end

function bic_window(mfccs::Array{Float64,2}, from::Int64, to::Int64)
    best = 100
    bestpos = 0
    scores = Array{AbstractFloat,1}()
    x = Array{Int64,1}()
    N, d = size(mfccs)

    if to > N
        to = N
    end

    if from < 1
        from = 1
    end

    for i = from+1:to-2
            b = BIC(mfccs, from, to, i)
            print(i/100.0, " ", b)
            if b == Inf
                b = 0
            elseif b == -Inf
                b = 0
            end
            # if b < best
            #     best = b
            #     bestpos = i
            # end
            push!(scores, b)
    end
    x = from:to
    scores = (scores - mean(scores))/std(scores)
    return  bestpos, scores, x
end

function run_bic(path::String, test::Bool = false)
    mfccs = Array{Float64,2}(wav2mfcc(path))
    #mfccs = wav2mfcc(path)
    N, d = size(mfccs)
    #N = length(mfccs)
    loop_boolean = true
    from_value = 1
    to_value = 201
    window_value = 150
    #to_value = N

    cut_times = Array{Float64,1}()
    score_list = Array{Vector,1}()
    t_score_list = Array{Vector,1}()

    tried_cut_times = Array{Float64,1}()
    tried_scores = Array{Float64,1}()

    tried_cut = Array{Float64,1}()
    tried_cut_times2 = Array{Float64,1}()


    sample_num = Int64
    x = UnitRange{Int64}
    old_time = 0.0
    while(loop_boolean)
        scores = Array{Array,1}()

        score = -1.0

        pos, t_scores = t2_window(mfccs, from_value, to_value)
        #pos, scores, x = bic_window(mfccs, from_value, to_value)
        push!(tried_cut, maximum(t_scores))
        index = findfirst(t_scores, maximum(t_scores))
        center_value = from_value + index - 1
        push!(tried_cut_times2, 0.020*center_value)
        
        if maximum(t_scores) > 800

            index = findfirst(t_scores, maximum(t_scores))
            center_value = from_value + index - 1

            push!(t_score_list, t_scores)

            score = BIC(mfccs, center_value-window_value, center_value+window_value, center_value)
            #pos, scores, x = bic_window(mfccs, center_value-window_value, center_value+window_value)
            #score = scores[window_value]
            #pos, scores, x = bic_window(mfccs, from_value, to_value)
            #push!(score_list, scores)

            push!(tried_cut_times, 0.020*center_value)
            push!(tried_scores, score)
            println("THIS IS THE SCORE: ", score)
        end

        if score > 0 && (0.020*center_value) - old_time > 2.0
            println("FØRSTE IF")
            from_value = to_value
            to_value += 50
            old_time = 0.020*center_value
            #current_start = x[1]
            #sample_num = findfirst(scores, score)
            #if scores[center_value-current_start+1] < 0
            println("TIME ", 0.020*center_value)
            push!(cut_times, 0.020*center_value)
            #push!(score_list, scores)   
            #end
        else
            #println("ANDRE IF")
            #println("SCORE ", score)
            #println("TRUE OR FASLE ", score == Inf)
            if to_value-from_value < 500
                to_value += 100
            else
                to_value += 50
            end
            if score == Inf
                println("KOM INN I INF IF")
                from_value = center_value + 1
            end
            if to_value-from_value > 1000
                from_value = to_value-200
                from_value = Int64(floor(from_value))
            end
        end

        if to_value >= N
            loop_boolean = false
            println("ENDTIME ", 0.020*N)
            push!(cut_times, 0.020*N)
        end

    end
    println("CUT TIMES ", cut_times)
    println("TRIED CUT TIMES ", tried_cut_times)
    if test
        return cut_times, t_score_list, score_list, tried_cut_times, tried_scores, tried_cut_times2, tried_cut
    end

    return cut_times
end
