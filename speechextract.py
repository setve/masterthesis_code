import math
import sys
import os

speakers1 = [
    ["00:00:53:21", "00:07:17:05", "Stefan Lõfven"],
    ["00:07:54:20", "00:14:30:00", "Ulf Kristersson"],
    ["00:15:01:05", "00:19:58:07", "Jimmy Åkesson"],
    ["00:20:21:09", "00:25:09:17", "Isabella Lövin"],
    ["00:25:32:13", "00:30:25:02", "Annie Lööf"],
    ["00:30:46:07", "00:35:29:12", "Jonas Sjöstedt"],
    ["00:35:48:23", "00:40:41:23", "Jan Björklund"],
    ["00:40:57:13", "00:45:46:03", "Andreas Carlson"]
]

speakers2 = [
    ["00:46:06:24", "00:47:56:15", "Stefan Löfven"],
    ["00:48:17:18", "00:49:15:15", "Ulf Kristersson"],
    ["00:49:22:24", "00:50:25:21", "Stefan Löfven"],
    ["00:50:32:13", "00:51:32:09", "Ulf Kristersson"],
    ["00:51:43:08", "00:52:37:10", "Stefan Löfven"],
    ["00:52:51:19", "00:53:46:10", "Jimmy Åkesson"],
    ["00:53:51:19", "00:54:53:09", "Stefan Löfven"],
    ["00:55:06:18", "00:56:02:13", "Jimmy Åkesson"],
    ["00:56:08:04", "00:57:12:02", "Stefan Löfven"],
    ["00:57:29:11", "00:58:29:23", "Annie Lööf"],
    ["00:58:36:07", "00:59:37:23", "Stefan Löfven"],
    ["00:59:42:09", "01:00:38:10", "Annie Lööf"],
    ["01:00:43:15", "01:01:41:07", "Stefan Löfven"],
    ["01:01:53:15", "01:02:49:08", "Jonas Sjöstedt"],
    ["01:02:52:15", "01:03:54:15", "Stefan Löfven"],
    ["01:04:11:22", "01:04:59:05", "Jonas Sjöstedt"],
    ["01:05:03:02", "01:06:03:00", "Stefan Löfven"],
    ["01:06:18:07", "01:07:12:21", "Jan Björklund"],
    ["01:07:15:12", "01:08:16:12", "Stefan Löfven"],
    ["01:08:23:23", "01:09:26:00", "Jan Björklund"],
    ["01:09:32:19", "01:10:31:03", "Stefan Löfven"],
    ["01:10:41:23", "01:11:31:04", "Andreas Carlson"],
    ["01:11:33:09", "01:12:32:20", "Stefan Löfven"],
    ["01:12:38:16", "01:13:35:21", "Andreas Carlson"],
    ["01:13:42:10", "01:14:38:14", "Stefan Löfven"],
    ["01:15:04:23", "01:16:48:16", "Ulf Kristersson"],
    ["01:17:10:15", "01:18:04:24", "Stefan Löfven"]
    # ["01:::", "01:::", ""],
]

# This is used for ubm trainingdata, videoen er debatt om skrogbrann
speakers3 = [
    ["00:00:48:00", "00:02:47:00", "Stefan Lõfven"],
    ["00:03:07:00", "00:09:07:00", "Morgan Johansson"],
    ["00:09:25:00", "00:15:19:00", "Beatrice Ask"],
    ["00:15:45:00", "00:21:42:00", "Roger Richtoff"],
    ["00:21:58:00", "00:27:43:00", "Marie Ferm"],
    ["00:28:03:00", "00:34:00:00", "Daniel Bäckström"],
    ["00:34:26:00", "00:40:21:00", "Lotta Johnsson Fornavre"],
    ["00:40:42:00", "00:46:25:00", "Allan Widman"],
    ["00:46:47:00", "00:52:45:00", "Mikael Oscarsson"],
    ["00:53:06:00", "00:57:03:00", "Morgan Johansson"],
    ["00:57:20:00", "01:01:14:00", "Beatrice Ask"],
    ["01:01:44:00", "01:05:39:00", "Roger Richtoff"],
    ["01:05:55:00", "01:09:02:00", "Marie Ferm"],
    ["01:09:20:00", "01:13:19:00", "Daniel Bäckström"],
    ["01:13:42:00", "01:17:03:00", "Lotta Johnsson Fornavre"],
    ["01:17:22:00", "01:21:06:00", "Allan Widman"],
    ["01:21:26:00", "01:25:29:00", "Mikael Oscarsson"],
    ["01:25:45:00", "01:27:44:00", "Morgan Johansson"],
    ["01:28:01:00", "01:30:00:00", "Beatrice Ask"],
    ["01:30:37:00", "01:32:37:00", "Roger Richtoff"],
    ["01:32:55:00", "01:34:56:00", "Marie Ferm"],
    ["01:35:17:00", "01:37:16:00", "Daniel Bäckström"],
    ["01:37:37:00", "01:39:28:00", "Lotta Johnsson Fornavre"],
    ["01:39:43:00", "01:40:55:00", "Allan Widman"],
    ["01:41:16:00", "01:43:15:00", "Mikael Oscarsson"],
    ["01:43:32:00", "01:45:32:00", "Morgan Johansson"]
]

# frågestund 7 juni (kan og bli brukt til treningsdata når eg skal teste med korte klipp. Dei fleste her er +-1 minut)
speakers4 = [
    ["00:00:28:00", "00:02:28:00", "Jessica Polfjärd"],
    ["00:02:49:00", "00:03:44:00", "Ylva Joahnsson"],
    ["00:05:01:00", "00:05:54:00", "Paula Bieler"],
    ["00:08:22:00", "00:09:26:00", "Lars Tysklind"],
    ["00:11:13:00", "00:11:41:00", "Karolina Skog"],
    ["00:11:50:00", "00:12:53:00", "Annika Qarlsson"],
    ["00:15:24:00", "00:16:07:00", "Edward Riedl"],
    ["00:17:56:00", "00:18:29:00", "Tomas Eneroth"],
    ["00:18:45:00", "00:19:44:00", "Eirik Slottner"],
    ["00:22:05:00", "00:22:55:00", "Serka Kose"],
    ["00:25:07:00", "00:25:52:00", "Rasmus Ling"],
    ["00:28:11:00", "00:28:58:00", "Sven-Olav Sallstrom"],
    ["00:31:29:00", "00:32:30:00", "Pia Nilson"],
    ["00:34:32:00", "00:35:21:00", "Rickard Nordin"],
    ["00:36:48:00", "00:37:45:00", "Lotta Finsstorp"],
    ["00:43:09:00", "00:44:01:00", "Anders Åkesson"],
    ["00:46:21:00", "00:47:17:00", "Lars Beckman"],
    ["00:49:25:00", "00:50:23:00", "Hillevi Larson"],
    ["00:52:04:00", "00:52:32:00", "Anne Ekstrom"],
    ["00:52:48:00", "00:53:44:00", "Per-Arne Haakonsen"],
    ["00:55:57:00", "00:56:45:00", "Jasenko Omanovic"]
]

# Frågestund 8 mars
speakers5 = [
    ["00:01:08:00", "00:02:02:00", "Camilla Waltersson Grönvall"],
    ["00:02:09:00", "00:03:08:00", "Annika strandhall"],
    ["00:03:13:00", "00:03:42:00", "Camilla Waltersson Grönvall"],
    ["00:03:47:00", "00:04:15:00", "Annika strandhall"],
    ["00:04:25:00", "00:05:13:00", "Sven Olof Sellstrom"],
    ["00:05:16:00", "00:06:12:00", "Gustav Fridolin"],
    ["00:06:15:00", "00:06:45:00", "Sven Olof Sellstrom"],
    ["00:06:45:00", "00:07:19:00", "Gustav Fridolin"],
    ["00:07:28:00", "00:08:28:00", "Peter Helander"],
    ["00:08:31:00", "00:09:32:00", "Ardelan Shekarabi"],
    ["00:09:36:00", "00:10:04:00", "Peter Helander"],
    ["00:10:08:00", "00:10:35:00", "Ardelan Shekarabi"],
    ["00:10:41:00", "00:11:09:00", "Peter Helander"],
    ["00:11:17:00", "00:12:10:00", "Karin Rogsjo"],
    ["00:12:23:00", "00:13:09:00", "Annika strandhall"],
    ["00:13:13:00", "00:13:40:00", "Karin Rogsjo"],
    ["00:13:43:00", "00:14:00:00", "Annika strandhall"],
    ["00:14:12:00", "00:15:01:00", "Bengt Eliasion"],
    ["00:16:07:00", "00:16:37:00", "Bengt Eliasion"],
    ["00:17:20:00", "00:17:40:00", "Emma Hendrikson"],
    ["00:18:43:00", "00:19:19:00", "Emma Hendrikson"],
    ["00:20:09:00", "00:21:06:00", "Yasmine Larson"],
    ["00:22:13:00", "00:22:34:00", "Yasmine Larson"],
    ["00:23:12:00", "00:24:05:00", "Lena Asplund"],
    ["00:25:13:00", "00:25:49:00", "Lena Asplund"],
    ["00:26:32:00", "00:27:32:00", "Emma Hult"],
    ["00:28:46:00", "00:29:14:00", "Emma Hult"],
    ["00:30:01:00", "00:31:00:00", "Marcus Wiechel"],
    ["00:31:58:00", "00:32:22:00", "Marcus Wiechel"],
    ["00:33:09:00", "00:34:12:00", "Anders W johnson"],
    ["00:35:06:00", "00:35:37:00", "Anders W johnson"],
    ["00:36:22:00", "00:37:15:00", "Azedeah Gustavson"],
    ["00:38:23:00", "00:38:43:00", "Azedeah Gustavson"],
    ["00:39:24:00", "00:40:12:00", "Maria Stendegrad"],
    ["00:40:16:00", "00:41:16:00", "Thomas Eneroth"],
    ["00:41:21:00", "00:41:47:00", "Maria Stendegrad"],
    ["00:41:50:00", "00:42:18:00", "Thomas Eneroth"],
    ["00:42:27:00", "00:43:26:00", "Oscar Sjøstedt"],
    ["00:43:29:00", "00:44:27:00", "Ardelan Shekarabi"],
    ["00:44:35:00", "00:45:02:00", "Oscar Sjøstedt"],
    ["00:45:55:00", "00:46:56:00", "Ida Drugge"],
    ["00:47:55:00", "00:48:27:00", "Ida Drugge"],
    ["00:49:16:00", "00:50:09:00", "Veronica Lindholm"],
    ["00:51:33:00", "00:52:32:00", "Magnus Oscarson"],
    ["00:53:47:00", "00:54:41:00", "Solveig Zander"],
    ["00:55:33:00", "00:56:23:00", "Jessica Rozengrants"],
    ["00:57:30:00", "00:58:17:00", "Jasenko Omanovic"],
    ["00:59:28:00", "01:00:28:00", "Johan Hultberg"],
    ["01:01:40:00", "01:02:42:00", "Eirik Bengtzboe"],
    ["01:03:54:00", "01:04:50:00", "Mattias Vepså"],
    ["01:06:06:00", "01:06:53:00", "Teresa Carvalho"]
]

# Debatt om förslag - Riktade statsbidrag till skolan
speakers6 = [
    ["00:00:24:00", "00:08:14:00", "Matilda Ernkrans"],
    ["00:08:33:00", "00:12:00:00", "Michael Svensson"],
    ["00:12:14:00", "00:15:55:00", "Robert Stenkvist"],
    ["00:16:10:00", "00:19:15:00", "Ulricka Carlsson"],
    ["00:19:32:00", "00:21:58:00", "Christer Nylander"],
    ["00:22:20:00", "00:25:47:00", "Annika Ecklund"]
]

# Debatt om förslag - Nya skatteregler för företagssektorn
speakers7 = [
    ["00:00:17:00", "00:05:16:00", "Dennis Diakurev"],
    ["00:05:33:00", "00:08:43:00", "Linda Snecker"],
    ["00:09:01:00", "00:17:15:00", "Olle Felten"],
    ["00:25:45:00", "00:30:40:00", "Jorgen Hellman"],
    ["00:31:02:00", "00:37:03:00", "Cecialia Widegren"],
    ["00:37:20:00", "00:40:24:00", "Rasmus Ling"],
    ["00:40:38:00", "00:45:20:00", "Mathias Sundin"],
    ["00:44:44:00", "00:52:13:00", "Larry Soder"]
]

# Debatt om förslag - Taxifrågor
speakers8 = [
    ["00:00:24:00", "00:07:34:00", "Jessica Rosencrantz"],
    ["00:07:51:00", "00:17:07:00", "Rossana Dinamarca"],
    ["00:17:28:00", "00:22:52:00", "Emma Carlson Lofdal"],
    ["00:23:09:00", "00:29:04:00", "Maria Olsson"],
    ["00:29:23:00", "00:33:14:00", "Tina Ghasemi"],
    ["00:33:31:00", "00:39:40:00", "Rickard Perssson"],
    ["00:39:58:00", "00:45:46:00", "Solveig Zander"]
]

# Debatt om förslag - Reformerade stöd till personer med funktionsnedsättning
speakers9 = [
    ["00:00:28:00", "00:06:23:00", "Carina Stahl Herrestedt"],
    ["00:06:42:00", "00:11:29:00", "Jimmy Stahl"],
    ["00:11:53:00", "00:16:34:00", "Anders Akeson"],
    ["00:16:56:00", "00:22:39:00", "Nina Lunstrom"],
    ["00:23:01:00", "00:28:13:00", "Robert Halef"],
    ["00:28:32:00", "00:36:06:00", "Karin Svenson Smith"],
    ["00:36:26:00", "00:40:40:00", "Leif Pettersson"]
]

# Debatt om förslag - En omreglerad spelmarknad
speakers10 = [
    ["00:00:29:00", "00:07:14:00", "Sara-Lena Bjalko"],
    ["00:07:33:00", "00:12:11:00", "Vasiliki Tsupolaki"],
    ["00:12:36:00", "00:22:37:00", "Olof Lavesson"],
    ["00:23:00:00", "00:29:46:00", "Peter Johnsson"],
    ["00:30:00:00", "00:35:31:00", "Agneta Borjeson"],
    ["00:35:50:00", "00:42:57:00", "Per Lodenius"],
    ["00:43:13:00", "00:52:48:00", "Bengt Eliasson"],
    ["00:53:07:00", "01:02:48:00", "Roland Ubult"],
    ["01:03:06:00", "01:10:46:00", "Ardelan Shekarabi"]
]

# Debatt om förslag - Allmänna helgdagar m.m.
speakers11 = [
    ["00:00:25:00", "00:06:00:00", "Jonas Millard"],
    ["00:06:30:00", "00:08:30:00", "Mia Sydow Molleby"],
    ["00:08:46:00", "00:17:51:00", "Andreas Norlen"],
    ["00:18:09:00", "00:24:03:00", "Bjorn von Sydow"],
    ["00:24:16:00", "00:26:38:00", "Agneta Borjeson"],
    ["00:26:57:00", "00:29:31:00", "Per-ingvar Johnsson"],
    ["00:29:50:00", "00:34:24:00", "Tina Acketoft"],
    ["00:34:39:00", "00:40:29:00", "Tuve Skånberg"],
    ["00:40:45:00", "00:46:59:00", "Hans Ekstrom"],
]

# Debatt om förslag - Resultatskrivelse om utvecklingssamarbete och humanitärt bistånd genom multilaterala organisationer
speakers12 = [
    ["00:00:26:00", "00:05:17:00", "Marcus Wiechel"],
    ["00:05:30:00", "00:09:47:00", "Maria Andersson Wilner"],
    ["00:10:01:00", "00:13:29:00", "Sofia Arkeltsen"],
    ["00:13:54:00", "00:19:15:00", "Emma Norhen"],
    ["00:19:30:00", "00:23:38:00", "Kerstin Lundgren"],
    ["00:23:54:00", "00:28:48:00", "Sofia Damm"],
]

# Debatt om förslag - Informationssäkerhet för samhällsviktiga och digitala tjänster
speakers13 = [
    ["00:00:25:00", "00:04:57:00", "Mikael Osrarsson"],
    ["00:05:21:00", "00:09:57:00", "Mikael Jansson"],
    ["00:10:12:00", "00:15:29:00", "Allan Widman"],
    ["00:15:45:00", "00:20:21:00", "Kalle Olsson"],
    ["00:20:46:00", "00:28:58:00", "Beatrice Ask"],
    ["00:29:20:00", "00:31:33:00", "Roger Richtoff"],
]

speakers14 = [
    ["00:00:00:00", "00:13:42:00", "alle"],
]


def ts2sec(timestamp, fps):
    invfps = 1.0 / fps
    split1 = timestamp.split(":")
    assert (len(split1) == 4)
    values = [int(strval) for strval in split1]
    # print(timestamp)
    assert (values[1] < 60)
    assert (values[2] < 60)
    assert (values[3] < math.ceil(fps))
    return 3600.0 * values[0] + 60.0 * values[1] + values[2] + invfps * values[3]


def read_speakers(speakers, fps):
    return [[ts2sec(sp[0], fps), ts2sec(sp[1], fps), sp[2]] for sp in speakers]


def generate_training_json(speakers, path_prefix, offset):
    txt = ""
    for i in range(len(speakers)):
        speaker = speakers[i]
        txt1 = '  ["' + speaker[2] + '", "wav", '
        txt2 = '"' + path_prefix + "/" + "clip_" + str(i + offset) + '.wav"]'

        txt3 = "\n"
        if i < len(speakers) - 1:
            txt3 = ",\n"
        txt += (txt1 + txt2 + txt3)
    return "[\n" + txt + "\n]"


def generate_extraction_commands(vidpath, speakers_txt, fps=25, outpath="./"):
    speakers = read_speakers(speakers_txt, fps)
    list_of_command = []
    for i in range(len(speakers)):
        speaker = speakers[i]
        t1 = speaker[0]
        t2 = speaker[1]
        cmd1 = "ffmpeg -y -i " + vidpath + ' -ss ' + str(t1) + ' -t ' + str(t2 - t1)
        cmd2 = " -vn -acodec pcm_s16le -f wav "
        cmd3 = outpath + "hallo_" + str(i) + ".wav"
        print(cmd1 + cmd2 + cmd3 + "\n")
        list_of_command.append(cmd1 + cmd2 + cmd3)


def generate_extraction_commands_from_timecodes(vidpath, time_codes, name, outpath="./", fps=25):
    list_of_command = []
    for time in time_codes:
        speaker = name
        t1 = time.split('-')[0]
        t2 = time.split('-')[1]
        cmd1 = "ffmpeg -y -i " + vidpath + ' -ss ' + t1 + ' -t ' + str(float(t2) - float(t1))
        cmd2 = " -vn -acodec pcm_s16le -f wav "
        cmd3 = outpath + name + "_" + str(t1) + ".wav"
        print(cmd1 + cmd2 + cmd3 + "\n")
        list_of_command.append(cmd1 + cmd2 + cmd3)
    return list_of_command


# if __name__ == "__main__":
#     if sys.argv[1] == "-cmds":
#         if len(sys.argv) > 4:
#             outpath = sys.argv[4]
#         else:
#             outpath = "./"
#         generate_extraction_commands(sys.argv[2], speakers14,
#                                      fps=int(sys.argv[3]), outpath=outpath)
#     elif sys.argv[1] == "-training":
#         json_txt = generate_training_json(speakers1, sys.argv[2], 0)
#         print(json_txt)
#     elif sys.argv[1] == "-test":
#         offset = len(speakers1)
#         json_txt = generate_training_json(speakers2, sys.argv[2], offset)
#         print(json_txt)
#     elif sys.argv[1] == "-ubmcmd":
#         outpath = "./ubm-alt/"
#         generate_extraction_commands(sys.argv[2], speakers13,
#                                      fps=int(sys.argv[3]), outpath=outpath)


generate_extraction_commands("ein_film.mp4", speakers14, fps=int(25))
