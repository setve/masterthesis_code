from lxml import etree
import copy, uuid, datetime
import os
import requests
import numpy as np
import pandas as pd
import speechextract
from julia import Julia


def check_for_known_persons(time_code_array):
    ivecs_list = os.listdir('./ivec_folder')
    database_entries = os.listdir('./database')
    database_ivec = []
    for entries in database_entries:
        name = str.split(entries, ".")[0]
        vector = np.load("./database/" + entries).tolist()
        d = {name: vector}
        database_ivec.append(d)

    for ivec_file in ivecs_list:
        df = pd.read_csv("./ivec_folder/" + str(ivec_file))

    treshhold = 0.5
    found_persons = {}
    check_calue = 0
    for index, current_row in df.iterrows():

        row_string = current_row[0][1:len(current_row[0]) - 1]
        current_ivec = [float(a) for a in row_string.split(",")]

        for current_entry in database_ivec:
            current_ivec_entry = list(current_entry.values())[0]
            current_name_entry = list(current_entry.keys())[0]

            score = np.dot(current_ivec, current_ivec_entry) / (
                    np.linalg.norm(current_ivec) * np.linalg.norm(current_ivec_entry))
            check_calue = check_calue + score
            if score >= treshhold:
                print("FOUND " + str(current_name_entry) + " AT TIME: " + str(current_row[1]))
                found_persons[current_row[1]] = current_name_entry
                for nested_list in time_code_array:
                    try:
                        nested_list.remove(current_row[1])
                        print("removed value from list")
                    except:
                        print("Value not found in the list")

    return time_code_array, found_persons


def clean_time_code_array(time_array):
    new_array = []
    for cluster in time_array:
        new_time_array = []
        while (cluster):
            time_code = cluster[0]
            new_time = checkTimes(time_code, cluster)
            if new_time != time_code:
                new_time_array.append(new_time)
            else:
                cluster.remove(time_code)
                new_time_array.append(time_code)
        new_array.append(new_time_array)
    return new_array


def checkTimes(time_code, time_array):
    print("HALLA DETTE R TIDEN: " + time_code)
    first_time_code = time_code.split('-')[0]
    second_time_code = time_code.split('-')[1]
    new_time_string = ""

    for times in time_array:
        check_time = times.split("-")[0]
        second_check_time = times.split("-")[1]
        if float(check_time) - float(second_time_code) == 0 and times != time_code:
            new_time_string = first_time_code + "-" + times.split("-")[1]
            time_array.remove(times)
            new_time_string = checkTimes(new_time_string, time_array)
            return new_time_string
        elif float(first_time_code) <= float(check_time) and float(second_check_time) <= float(
                second_time_code) and times != time_code:
            new_time_string = time_code
            time_array.remove(times)
            new_time_string = checkTimes(new_time_string, time_array)
            return new_time_string
        elif float(first_time_code) == float(second_check_time) and times != time_code:
            new_time_string = check_time + "-" + second_time_code
            time_array.remove(times)
            new_time_string = checkTimes(new_time_string, time_array)
            return new_time_string
    return time_code


def add_graphic_for_person_new(root_element, element, graphic_element, time_code_array, person_number):
    if type(time_code_array) == str:
        start_time = time_code_array.split('-')[0]
        duration = float(time_code_array.split('-')[1]) - float(start_time)
        start_element = copy.copy(element)
        start_graphic_element = copy.copy(graphic_element)
        root_element.append(
            build_graphic_tag_new(start_element, start_graphic_element, duration, start_time, person_number))
    else:
        for time_code in time_code_array:
            start_time = time_code.split('-')[0]
            duration = float(time_code.split('-')[1]) - float(start_time)
            start_element = copy.copy(element)
            start_graphic_element = copy.copy(graphic_element)
            # build_element = build_graphic_tag_new(start_element, start_graphic_element)
            # build_graphic_tag_new(start_element, start_graphic_element)
            root_element.append(
                build_graphic_tag_new(start_element, start_graphic_element, duration, start_time, person_number))

    return root_element


def build_graphic_tag_new(element, template_element, duration, start=0, number=1):
    atom_prefix = '{http://www.w3.org/2005/Atom}'
    vaext_prfix = '{http://www.vizrt.com/atom-ext}'
    unique_id = str(uuid.uuid4()).upper()
    current_date = datetime.datetime.now()
    # string_lenght = len("Subject " + str(number))

    if type(number) == int:
        title = "Subject " + str(number)
    elif type(number) == str:
        title = number

    for tag in template_element:
        element.append(tag)

    element.find(atom_prefix + 'id').text = str(unique_id)
    #  element.find(atom_prefix + 'updated').text = str(current_date)
    element.find(atom_prefix + 'title').text = title + "/8"
    element.find(atom_prefix + 'content')[0][1][0].text = title
    element.find(atom_prefix + 'content')[0][9][0].text = title

    element.find(vaext_prfix + 'start').text = str(start)
    element.find(vaext_prfix + 'duration').text = str(duration)
    element.tail = template_element.tail
    element.text = template_element.text

    return element


def get_timecodes_from_story(
        url='http://desktop-5km6ak4:9724/resource/urn%3Auuid%3A22678EA6-5FB1-4418-8D62-DF7FBA417AAF/content',
        save_folder=r'C:\Users\141447\Desktop\Kode-Master\saved_training_timecodes'):
    resp = requests.get(
        'http://desktop-5km6ak4:9724/resource/urn%3Auuid%3A22678EA6-5FB1-4418-8D62-DF7FBA417AAF/content')
    tree = etree.ElementTree(etree.XML(resp.text))
    tree.write('test666.xml')
    root = tree.getroot()
    feed_root = root[1][0][0]
    media = feed_root[1][3][0]
    persons_timecodes = {}
    story_name = root[2][0].text
    story_name = story_name.replace(" ", "_")

    for el in feed_root[3:]:
        name = el[1].text
        name = name.split('/')[0]
        name = name.replace(" ", "_")
        if 'Subject' not in name:
            time = el[10].text
            duration = el[11].text
            timecode = str(time) + "-" + str(float(time) + float(duration))
            try:
                persons_timecodes[name].append(timecode)
            except KeyError:
                persons_timecodes[name] = [timecode]
            print(persons_timecodes)

    os.chdir(save_folder)

    for key, value in persons_timecodes.items():
        newpath = save_folder + "\\" + key
        if not os.path.exists(newpath):
            os.makedirs(newpath)
            os.makedirs(newpath + "\\original_files")
        os.chdir(newpath)
        f = open(story_name + ".txt", "w+")
        for time in value:
            f.write(time + "\n")
        f.close()
        os.chdir('..')
    os.chdir('..')
    generate_trainingdata()


def generate_trainingdata(main_folder=r'C:\Users\141447\Desktop\Kode-Master\saved_training_timecodes'):
    os.chdir(main_folder)
    list_of_folders = os.listdir()
    for folder in list_of_folders:
        os.chdir(folder)
        filepaths = [i for i in os.listdir() if ".txt" in i]

        for file in filepaths:
            fileName = file.split(".")[0]
            timecodes = [line.rstrip('\n') for line in open(file)]
            filepath = os.getcwd() + "\\" + "original_files" + "\\" + fileName + ".wav"
            outpath = os.getcwd() + "\\" + "trainingdata\\"

            if not os.path.exists(outpath):
                os.makedirs(outpath)

            if not os.path.exists(filepath):
                filepath = os.getcwd() + "\\" + "original_files" + "\\" + fileName + ".mp4"

            list_of_commands = speechextract.generate_extraction_commands_from_timecodes(filepath, timecodes, fileName,
                                                                                         outpath)

            for command in list_of_commands:
                os.system(command)
            os.chdir("..")


def run_on_url(url):
    os.chdir("C:/Users/141447/Desktop/Kode-Master")
    r = requests.get(url)
    print(r)

    template_tree = etree.parse('template_xml.xml')
    template_feed_root = template_tree.getroot()[1][0][0]
    atom_tag = template_feed_root[2].tag
    graphic_element = copy.copy(template_feed_root[2])

    tree = etree.ElementTree(etree.XML(r.text))
    root = tree.getroot()
    feed_root = root[1][0][0]
    print(etree.tostring(feed_root))
    # feed_root.remove(feed_root[2])
    tree.write('test6.xml')

    stor_array = []

    for file in os.listdir('timecode_folder'):
        f = open("./timecode_folder/" + file, "r")
        contents = f.read()
        contents = contents[:-1]
        time_codes = contents.split(',')
        stor_array.append(time_codes)

    print(stor_array)
    stor_array, persons_dict = check_for_known_persons(stor_array)
    stor_array = clean_time_code_array(stor_array)

    print(stor_array)

    new_element = etree.Element(atom_tag, nsmap=graphic_element.nsmap)
    for index, kvar_array in enumerate(stor_array):
        feed_root = add_graphic_for_person_new(feed_root, new_element, graphic_element, kvar_array, index)
    for key, value in persons_dict.items():
        feed_root = add_graphic_for_person_new(feed_root, new_element, graphic_element, key, value)

    r = requests.put(url,
                     data=etree.tostring(tree, pretty_print=True),
                     headers={'Content-Type': 'application/vnd.vizrt.payload+xml; role=story'})
    print(r)


# url = 'http://desktop-5km6ak4:9724/resource/urn%3Auuid%3ADF1FA545-7C0C-46C8-8AA5-62F2C2C3C77E/content'
#url = 'http://desktop-5km6ak4:9724/resource/urn%3Auuid%3A22678EA6-5FB1-4418-8D62-DF7FBA417AAF/content'
# print("HALLA")
# r = requests.get(url)
# print(r)
#
# template_tree = etree.parse('template_xml.xml')
# template_feed_root = template_tree.getroot()[1][0][0]
# atom_tag = template_feed_root[2].tag
# graphic_element = copy.copy(template_feed_root[2])
#
# tree = etree.ElementTree(etree.XML(r.text))
# root = tree.getroot()
# feed_root = root[1][0][0]
# print(etree.tostring(feed_root))
# # feed_root.remove(feed_root[2])
# tree.write('test6.xml')
#
# stor_array = []
#
# for file in os.listdir('timecode_folder'):
#     f = open("./timecode_folder/" + file, "r")
#     contents = f.read()
#     contents = contents[:-1]
#     time_codes = contents.split(',')
#     stor_array.append(time_codes)
#
# print(stor_array)
# stor_array, persons_dict = check_for_known_persons(stor_array)
# stor_array = clean_time_code_array(stor_array)
#
# print(stor_array)
#
# new_element = etree.Element(atom_tag, nsmap=graphic_element.nsmap)
# for index, kvar_array in enumerate(stor_array):
#     feed_root = add_graphic_for_person_new(feed_root, new_element, graphic_element, kvar_array, index)
# for key, value in persons_dict.items():
#     feed_root = add_graphic_for_person_new(feed_root, new_element, graphic_element, key, value)

# r = requests.put(url,
#                  data=etree.tostring(tree, pretty_print=True),
#                  headers={'Content-Type': 'application/vnd.vizrt.payload+xml; role=story'})

#print(r.status_code)

#data = etree.tostring(tree, pretty_print=True)

# get_timecodes_from_story()

run_on_url('http://desktop-5km6ak4:9724/resource/urn%3Auuid%3A22678EA6-5FB1-4418-8D62-DF7FBA417AAF/content')

