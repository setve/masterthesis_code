import WAV
import MFCC
import GaussianMixtures
using CSV
using JLD
using IVectors
using FileIO
using MultivariateStats
using NPZ
using DataFrames
using Clustering
using PyCall
include("bic.jl")


import JSON

struct GMMInfo 
  name::AbstractString
  path::AbstractString
  gmm::GaussianMixtures.GMM
end

struct SVInfo 
  name::AbstractString
  sv::Vector
end

struct IVecInfo 
  name::AbstractString
  ivec::Vector
end

gmms = Array{GMMInfo,1}()
supervectors = Array{SVInfo,1}()
ivectors = Array{IVecInfo,1}()
pcaModel = MultivariateStats.PCA
whiteningModel = MultivariateStats.Whitening

function extract_audio(path::AbstractString, out_path::AbstractString="./audio_from_video.wav")
	run(`ffmpeg -y -i $path -vn -acodec pcm_s16le -f wav $out_path`)
	return out_path
end

#CHeck for known people in database(not used)
function check_for_know_people(ivec_dict::Dict)
	database_entries = readdir("./database")
	database_ivec = Vector{Dict}()
	for entries in database_entries
		name = split(entries, ".")[1]
		vector = load("./database/" * entries)
		d = Dict(vector => name)
		push!(database_ivec, d)
	end

	treshhold = 0.9
	for current_vector in keys(ivec_dict)
		for current_entry in keys(database_ivec)
			score = cosdist(current_vector, current_entry)

			if score >= treshhold
				println("FOUND " * database_ivec[current_entry] * " AT TIME: " * database_ivec[current_vector])
			end
		end
	end
end


#Divide an audiclip into samples of length sample_length
function sampling(path::AbstractString, sample_length::Integer, fileFormat::AbstractString, BIC::Bool = true)
	println(fileFormat)

	if fileFormat == "wav"
		y, sampling_frequency = WAV.wavread(path)
	elseif fileFormat == "mp4"
		path = extract_audio(path)
		y, sampling_frequency = WAV.wavread(path)
	end

	sample_array = y[:,1]
	sample_array = (sample_array - mean(sample_array))/std(sample_array)
	samples = Vector{Array}()

	if BIC == false
		number_of_seconds = length(sample_array)/sampling_frequency
		number_of_samples = number_of_seconds/sample_length
		number_of_samples = Int64(floor(number_of_samples))
		
		start_index = 0
		end_index = 0
		for sample = 0:number_of_samples
			start_index = Int64(sample*(sampling_frequency*sample_length)+1)
			end_index = Int64(start_index + (sampling_frequency*sample_length)-1)
			if end_index < length(sample_array)
				push!(samples, sample_array[start_index:end_index])
			end
		end
	else
		time_codes = run_bic(path)
		sort!(time_codes)
		old_time = 1
		for times in time_codes
			println(Int64(floor(times*sampling_frequency)))
			push!(samples, sample_array[old_time:Int64(floor(times*sampling_frequency))])
			old_time = Int64(floor((times*sampling_frequency)))
		end
	end
	return samples, time_codes
end

function cluster_kmean(k::Integer, path::AbstractString, fileFormat::AbstractString = "wav", from_python::Bool=false, sample_length::Integer=2, ittr_number::Integer=500)
	#Makes MFCC's and does the setup for the clustering
	samples, time_codes = sampling(path, sample_length, fileFormat)
	println("TIMECODES ", time_codes)
	ubm = GMMInfo("UBM", "", load("ubm-alt-1024-delta-mvn-stand.jld", "ubm-alt-1024-delta-mvn-stand"))
	ie = load("IExtractor4.iex")
	cut_off = true
	ivec_dict = Dict{Vector, String}()
	centroid_dict = Dict{Int64, Array}()
	cluster_dict = Dict{Int64, Array}()
	second_counter = 0
	vec_length = 0
	old_time = 0
	for (index,sample) in enumerate(samples)		
		mfccs, power, info= MFCC.feacalc(sample; wintime=0.025, steptime=0.02, augtype=:delta, normtype=:mvn, numcep=13, defaults=:rasta)
		mfccs = convert(Array{Float64, 2}, mfccs)
		ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfccs))
		vec_length = length(ivec)
		if !(any(isnan,ivec))
			println("OLD TIME ", old_time)
			time_string =  string(old_time) * "-" * string(time_codes[index])
			println("TIME STRING ", time_string)
			#println("SECOND ", second_counter)
			ivec_dict[ivec] = time_string
			old_time = time_codes[index]
		end
		second_counter += sample_length
	end

	k_means_matrix = Array{Float64, 2}(400, 0)

	for vectors in keys(ivec_dict)
		k_means_matrix = cat(2, k_means_matrix, vectors)
	end
	n, d = size(k_means_matrix)
	if d==k
		k_means_matrix = cat(2, k_means_matrix, collect(keys(ivec_dict))[1])
	end
	println("NUMBER OF CLUSTERS: ", k)
	println("NUMBER OF vectors: ", length(ivec_dict))
	println("SIZE OF VECTOR: ", size(k_means_matrix))
	R = kmeans(k_means_matrix, k; maxiter=200, display=:iter, init=:kmcen)
	assign = assignments(R)
	c = counts(R)

	println(assign)
	println(c)

	return_array = Array{Array,1}()

	for i = 1:length(c)
	    push!(return_array, Array{String,1}())
	end

	for i = 1:length(assign)
		print(i)
		push!(return_array[assign[i]], ivec_dict[k_means_matrix[:,i]])
	end

	cd("./ivec_folder")
		df = DataFrame(vector=Array[], timecode=String[])
		println(size(df))
		for element in ivec_dict
			push!(df, (element))
		end
		println(size(df))
		print(df)
		CSV.write("ivecs.csv", df)
	cd("..")

	timecode_path = "./timecode_folder"
	cd(timecode_path)
	filename_list = readdir()
	for file in filename_list
		println(file)
		rm(file, force=true)
	end
	for (index, array) in enumerate(return_array)
		name = String("timecodes_" * string(index-1) * ".txt")
		open(name, "w") do f
			for time_code in array
			   write(f, time_code*",")  
			end
       end
		print_array = Array{String, 1}()
		append!(print_array, array)
	end
	cd("..")
	println("No ER DU FERDIG MED JULIA")
	if from_python
		return "HEISANN"
	else
		run(`python generate_xml.py`)
		println("NO ER DU FERDIG MED PYTHON")
	end
end

#Algorithm for clustring the samples
function cluster(k::Integer, name::AbstractString, sample_length::Integer, ittr_number::Integer=500)
	#Makes MFCC's and does the setup for the clustering
	samples = sampling(name, sample_length)
	ubm = GMMInfo("UBM", "", load("ubm-alt-1024-delta-mvn-stand.jld", "ubm-alt-1024-delta-mvn-stand"))
	ie = load("IExtractor4	.iex")
	cut_off = true
	ivec_dict = Dict{Vector, String}()
	centroid_dict = Dict{Int64, Array}()
	cluster_dict = Dict{Int64, Array}()
	second_counter = 0
	vec_length = 0
	for (index,sample) in enumerate(samples)		
		mfccs, power, info= MFCC.feacalc(sample; wintime=0.025, steptime=0.01, augtype=:delta, normtype=:mvn, numcep=13, defaults=:rasta)
		mfccs = convert(Array{Float64, 2}, mfccs)
		ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfccs))
		vec_length = length(ivec)
		if !(any(isnan,ivec))
			time_string =  string(second_counter) * "-" * string(second_counter+sample_length)
			println("TIME STRING ", time_string)
			println("SECOND ", second_counter)
			ivec_dict[ivec] = time_string
		end
		second_counter += sample_length
	end
	
	for i = 1:k
		centroid_dict[i] = rand(vec_length)
	end
	
	stop_counter = 1
	
	while(cut_off)
		old_cluster_dict = copy(cluster_dict)

		for i = 1:k
			cluster_dict[i] = []
		end
		#Goes trough each ivector and assigns it to a cluster
		for current_vector in keys(ivec_dict)
			max_score = -Inf
			max_score_key = 0
			println(length(centroid_dict))
			for centroid_key in keys(centroid_dict)
				score = cosdist(current_vector, centroid_dict[centroid_key])
				if score > max_score
					max_score = score
					max_score_key = centroid_key
				end
			end
			push!(cluster_dict[max_score_key], current_vector)
		end
		
		#Recalculate centroid
		for key in keys(centroid_dict)
			println(typeof(cluster_dict[key]))
			println("lengde ", length(cluster_dict[key]))
			centroid_dict[key] = mean(cluster_dict[key])
		end
		#

		
		#println(values(old_cluster_dict) == values(cluster_dict))
		if stop_counter == ittr_number || old_cluster_dict == cluster_dict
			cut_off = false
			println("SHIT GOT CUT OFF")
			if old_cluster_dict == cluster_dict
				println("DEI ER LIKE")
			end
		end
		println(stop_counter)
		stop_counter += 1
	end
	
	#Goes trough each cluster and builds up and array containing arrays with the time_strings from each cluster
	return_array = Array{Array,1}()
	for key in keys(cluster_dict)
		string_array = Array{String,1}()
		for vector in cluster_dict[key]
			push!(string_array, ivec_dict[vector])
		end
		push!(return_array, string_array)
	end

	#Saves the ivector and the corresponding timecodes
	cd("./ivec_folder")
		df = DataFrame(vector=Array[], timecode=String[])
		println(size(df))
		for element in ivec_dict
			push!(df, (element))
		end
		println(size(df))
		print(df)
		CSV.write("ivecs.csv", df)
	cd("..")
	
	#Saves the timecodes to text files
	cd("./timecode_folder")
	final_array = return_array
	for (index, array) in enumerate(final_array)
		name = String("timecodes_" * string(index-1) * ".txt")
		open(name, "w") do f
			for time_code in array
			   write(f, time_code*",")  
			end
       end
		print_array = Array{String, 1}()
		append!(print_array, array)

	end
	cd("..")
	#run(`python generate_xml.py`)
	println(return_array)
	println(stop_counter)
	#return return_array
end


#Computes and saves an ivector extractor
function make_ivector_extractor(ubm::GMMInfo=GMMInfo("UBM", "", load("ubm-alt-1024-delta-mvn-stand.jld", "ubm-alt-1024-delta-mvn-stand")), ubmdir::String="ubm-alt"; ncomp=400)
    cd(ubmdir)
	filename_list = readdir()
    mfcc_list = []
	model_training_mfcc = Array{Float64,2}()
	println(typeof(model_training_mfcc))
    for filename in filename_list
		print(filename)
		y, fs = WAV.wavread(filename)
    	samples = y[:,1]
    	samples = (samples - mean(samples))/std(samples)
        #mfccs, power, info= MFCC.feacalc(filename, :diarization, wintime=0.025, steptime=0.01, augtype=:delta)
        mfccs, power, info= MFCC.feacalc(samples; wintime=0.025, steptime=0.01, augtype=:delta, normtype=:mvn, numcep=13, defaults=:rasta)
		mfccs = convert(Array{Float64, 2}, mfccs)
        push!(mfcc_list, mfccs)
    end
	cd("..")
	css = map((data)->GaussianMixtures.CSstats(ubm.gmm, data), mfcc_list)
    ie = IVectors.IExtractor(css, ncomp; nIter=3)
	save("IExtractor4.iex", ie)
end

#Computes an ivector for each audiclip(not used)
function train_ivec(json_path::AbstractString="json.txt", wavpath::AbstractString="C:\\Users\\141447\\Desktop\\Kode-Master", gmms::Array{GMMInfo}=gmms)
	ie = load("IExtractor3.iex")
	#gmminfo = GMMInfo("UBM", "", load("saved-ubm(fraag+skog).jld", "size=600, fraagstund+noko skogbrann"))
	#gmminfo = GMMInfo("UBM", "", load("ubm-alt.jld", "ubm-alt"))
	gmminfo = GMMInfo("UBM", "", load("ubm-alt-1024-delta.jld", "ubm-alt-1024-delta"))
	push!(gmms, gmminfo)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
	for speaker in json
		println("mfccs begin")
		path = wavpath * "\\" * speaker[3]
		y, fs = WAV.wavread(speaker[3])
		samples = y[:,1]
		#mfccs2, power, info = MFCC.mfcc(samples, Float64(fs), wintime=0.025, steptime=0.01)
		mfccs, power, info= MFCC.feacalc(path, :diarization, wintime=0.025, steptime=0.01, augtype=:delta)
		mfccs = convert(Array{Float64, 2}, mfccs)
		ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(gmminfo.gmm, mfccs))
		#ivec = transform(pcaModel, ivec)
		iv = IVecInfo(speaker[1], ivec)
		push!(ivectors, iv)
	end
	ivectors
end

#Computes an ivector for a give audioclip
function make_ivector(ubm, ie::IVectors.IExtractor, path::AbstractString)
    y, fs = WAV.wavread(path)
    samples = y[:,1]
    samples = (samples - mean(samples))/std(samples)
    mfccs, power, info= MFCC.feacalc(samples; wintime=0.025, steptime=0.01, augtype=:delta, normtype=:mvn, numcep=13, defaults=:rasta)
	mfccs = convert(Array{Float64, 2}, mfccs)
    ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfccs))
	if (any(isnan,ivec))
		println("THIS IVECTOR CONTAINS NAN VALUES: ", ivec)
	end
	ivec
end

#Computes an ivector for a given audioclip and saves it
function make_and_save_ivector(path::AbstractString, name::AbstractString,ie::IVectors.IExtractor = load("IExtractor4.iex"), ubm::GMMInfo=GMMInfo("UBM", "", load("ubm-alt-1024-delta-mvn-stand.jld", "ubm-alt-1024-delta-mvn-stand")))
    y, fs = WAV.wavread(path)
    samples = y[:,1]
    samples = (samples - mean(samples))/std(samples)
    mfccs, power, info= MFCC.feacalc(samples; wintime=0.025, steptime=0.01, augtype=:delta, normtype=:mvn, numcep=13, defaults=:rasta)
	mfccs = convert(Array{Float64, 2}, mfccs)
    ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfccs))
	if (any(isnan,ivec))
		println("THIS IVECTOR CONTAINS NAN VALUES: ", ivec)
	end
	cd("./database")
	npzwrite(name * ".npz",ivec)
	cd("..")
	ivec
end

#Computes an ivector for a given audioclip and saves it
function make_and_save_ivector_from_timecodes(path::AbstractString="C:\\Users\\141447\\Desktop\\Kode-Master\\saved_training_timecodes", ie::IVectors.IExtractor = load("IExtractor4.iex"), ubm::GMMInfo=GMMInfo("UBM", "", load("ubm-alt-1024-delta-mvn-stand.jld", "ubm-alt-1024-delta-mvn-stand")))
    cd(path)
	folder_list = readdir()
	model_training_mfcc = Array{Float64,2}()

    for foldername in folder_list
    	cd(".\\"*foldername*"\\trainingdata")
    	mfcc_list = []
    	filename_list = readdir()
    	for filename in filename_list
			print(filename)
			y, fs = WAV.wavread(filename)
	    	samples = y[:,1]
	    	samples = (samples - mean(samples))/std(samples)
	        mfccs, power, info= MFCC.feacalc(samples; wintime=0.025, steptime=0.01, augtype=:delta, normtype=:mvn, numcep=13, defaults=:rasta)
			mfccs = convert(Array{Float64, 2}, mfccs)
			if isempty(mfcc_list)
				mfcc_list = mfccs
			else
				mfcc_list = vcat(mfccs, mfcc_list)
			end
	    end

	    cd("C:\\Users\\141447\\Desktop\\Kode-Master\\database")
	    ivec = IVectors.ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfcc_list))
		
		if (any(isnan,ivec))
			println("THIS IVECTOR CONTAINS NAN VALUES: ", ivec)
		end
		npzwrite(foldername * ".npz", ivec)
		cd(path)
    end
end

#Computes ivectors for a list of audiclips
function make_ivectors(ie::IVectors.IExtractor = load("IExtractor4.iex"), json_path::AbstractString="json.txt")
	ubm = GMMInfo("UBM", "", load("ubm-alt-1024-delta-mvn-stand.jld", "ubm-alt-1024-delta-mvn-stand"))
    f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
    for speaker_rec in json
        speaker = speaker_rec[1]
        println(speaker)
        ivec = make_ivector(ubm, ie, speaker_rec[3])
		ivecinf = IVecInfo(speaker, ivec)
        push!(ivectors, ivecinf)
    end
    ivectors
end

function test_IV(json_path::AbstractString="json_test.txt", ivmodels::Array{IVecInfo}=ivectors)
	f = open(json_path)
    content = read(f, String)
    close(f)
    json = JSON.parse(content)
	perc = 0
	for speaker in json
		name, score, scores_list = classify_iv(speaker[3], ivmodels)
		if name == speaker[1]
			println("KORREKT!!, navn: ", name, " Score: ", score)
			println("Average score ", sum(scores_list)/length(ivmodels))
			println("Score list ", scores_list)
			println(" ")
			perc += 1
		else
			println("Feil! Klassifisert som: ", name, " Score: ", score)
			println("Korrekt svar er: ", speaker[1])
			println("Average score ", sum(scores_list)/length(ivmodels))
			println("Score list ", scores_list)
			println(" ")
		end
	end
	println("antall korrekt er: ", perc)
	println("Prosent korrekt er: ", (perc/length(json)))
	println(length(json))
end

function classify_iv(path::AbstractString, ivmodels::Array{IVecInfo}=ivectors)
	ie = load("IExtractor4.iex")
	ubm = GMMInfo("UBM", "", load("ubm-alt-1024-delta-mvn-stand.jld", "ubm-alt-1024-delta-mvn-stand"))
	y, fs = WAV.wavread(path)
    samples = y[:,1]
    samples = (samples - mean(samples))/std(samples)
	mfccs, power, info= MFCC.feacalc(samples; wintime=0.025, steptime=0.01, augtype=:delta, normtype=:mvn, numcep=13, defaults=:rasta)
	mfccs = convert(Array{Float64, 2}, mfccs)
	unknown_ivec = ivector(ie, GaussianMixtures.CSstats(ubm.gmm, mfccs))

	scores_list = Array{Float64,1}()
	for vector in ivmodels
		score = cosdist(vector.ivec, unknown_ivec)
		push!(scores_list, score)
	end
    pos = indmax(scores_list)
    ivmodels[pos].name, scores_list[pos], scores_list
end

function Mypcasvd(vec::Array{Float64,1}, dims::Int64)
	#mean = Float64()
	mean = Float64(Base.mean(vec))
	#new_vec = centralize(vec, mean)
	new_vec = vec-mean

	new_vec = reshape(new_vec, 1, length(new_vec))
	#new_vec = reshape(new_vec, length(new_vec), 1)

	SVD = svdfact(new_vec)
	println("YO ", SVD[:Vt])

	PC = SVD.U[1] * SVD.S[1]

	reduced_matrix = PC * SVD.V[:,dims]

	return PC, reduced_matrix
end

function Mypcacov(vec::Array{Float64,1}, dims::Int64)
	mean = Float64(Base.mean(vec))
	vec = vec-mean

	#new_vec = reshape(new_vec, 1, length(new_vec))
	#new_vec = reshape(new_vec, length(new_vec), 1)

	C = Base.cov(vec)
	println(C)
	C = reshape([C], 1, length([C]))
	V,D = Base.eig(C)

	println("C ", C)
	println("V ", V)
	println("D ", D)

	newdata = V * vec'
	newdata = newdata'
	println(newdata)

end